<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CategorieController;
use App\Http\Controllers\exoController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Models\Categorie;
use App\Models\User;
use Symfony\Component\Finder\Iterator\CustomFilterIterator;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('main');
})->name('home');

Route::get('home', function () {
    //declarer la variable name et lui affecter une valeur
    $name= "Maelle kponou";
    $description= "nous somme en cours";
    return view('home', [
        'name'=> $name,
        'desc'=> $description
    ]);

});

// declaration de route depuis un controller
Route:: get('fromcontroller', [HomeController::class,"index"]);
Route:: get('presentation', [HomeController::class,"presentation"]);
Route:: get('from', [HomeController::class,"customer"]);
Route:: get('test', [exoController::class,"coucou"]);
route:: get('client',[exoController::class,"customer"]);


route:: get('article/list',[ArticleController::class,"index"])->name('article.index');
route:: get('article/create',[ArticleController::class,"create"])->name('article.create');
route:: post('article/store',[ArticleController::class,"store"])->name('article.store');

route:: get('category/liste',[CategorieController::class,"index"])-> name('category.index');
route:: get('category/create',[CategorieController::class,"create"])-> name('category.create');
route:: post('category/store', [CategorieController::class,"store"])-> name('category.store');

route:: get('user/liste', [UserController::class,"index"])-> name('user.index');
route:: get('user/create',[UserController::class,"create"])->name('user.create');
route:: post('user/store', [UserController::class,"store"])-> name('user.store');


route:: get('article/{article}/edit', [ArticleController::class,"edit"])-> name('article.edit');
route:: post('article/{article}/update', [ArticleController::class,"update"])-> name('article.update');
route:: get('article/{article}/delete', [ArticleController::class,"delete"])-> name('article.delete');

route:: get('category/{categorie}/edit', [CategorieController::class,"edit"])-> name('category.edit');
route:: post('category/{categorie}/update', [CategorieController::class,"update"])-> name('category.update');
route:: get('category/{categorie}/delete', [CategorieController::class,"delete"])-> name('category.delete');



