@extends('main')

@section('title')
Enregistrement d'un article
@endsection
@section('name')
Enregistrement d'un article
@endsection
@section('content')
<div class="container mb-5">
    <form action="{{route('article.store')}}" method="post">
        @csrf
    <div class="col-6">
        <div class="my-3">

            <label for="">Catégorie</label>
            <select name="category" class="form-select" aria-label="Default select example">
                <option>selectionner une categorie</option>
                    @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->intitule}}</option>
                    @endforeach
            </select>
        </div>

          <label for="">Article</label>
          <input name="article" class="form-control form-control-sm">
    </div>
    <div class="my-3">

        <button type="submit" class="btn btn-success"><i class="bi bi-floppy2"></i> Enregistrer</button>
        <button class="btn btn-danger"><i class="bi bi-x-octagon-fill"></i> Annuler</button>
    </div>

</form>
</div>

@endsection

