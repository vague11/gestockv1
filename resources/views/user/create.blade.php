<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>

    <div class= "container mb-5">
        <h1 class="mt-4 mb-5">Enregistrement des utilisateurs</h1>
        <form action="{{route('user.store')}}" method="post">
            @csrf
            <div class="col-6">
                <div class="my-3">

                    <label for="">Nom complet</label>
                    <input name="name" class="form-control form-control-sm">

                    <label for="">Adresse mail</label>
                    <input name="mail" class="form-control form-control-sm">

                    <label for="">Mot de passe</label>
                    <input name="password" class="form-control form-control-sm">
                </div>
            </div>
            <div class="my-3">

                <button type="submit" class="btn btn-success">Enregistrer</button>
                <button class="btn btn-danger">Annuler</button>
            </div>

        </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
</script>
</body>
