

@extends('main')

@section('title')
Liste des catégories
@endsection
@section('name')
Liste des categories
@endsection
@section('content')
<div class= "container mt-5">
    <div class="row mb-5">
        <div class= "col-4">
            <input type="text"class="form-control form-control-sm col-6" id="formGroupExampleInput"
            placeholder="Recherche">
        </div>
        <div class="col-2">

            <a href="" class= "btn btn-primary btn-sm">RECHERCHE</a>
        </div>
    </div>



    <div class="">
        <a href="{{route('category.create')}}" class= "btn btn-success btn-sm mb-3"><i class="bi bi-plus "></i> Nouveau</a>
        <table class="table table-hover table-bordered.">
            <thead>
                <tr>
                    <td><h4>Catégorie</h4></td>
                    <td class="text-end"> <h4>Actions</h4></td>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                    <tr>
                        <td>{{ $category->intitule }}</td>
                        <td class="text-end">
                            <a href="{{ route('category.edit',['categorie'=>$category->id]) }}" class= "btn btn-primary btn-sm"><i class="bi bi-pencil-square"></i>modifier</a>
                            <a href="{{ route('category.delete',['categorie'=>$category->id])}}" class= "btn btn-danger btn-sm"><i class="bi bi-trash"></i>supprimer</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
