
@extends('main')

@section('title')
Enregistrement d'une catégorie
@endsection
@section('name')
Enregistrement d'une catégorie
@endsection
@section('content')
<div class= "container mb-5">
    <form action="{{route('category.store')}}" method="post">
        @csrf
        <div class="col-6">
            <div class="my-3">

                <label for="">Catégorie</label>
                <input name="category" class="form-control form-control-sm">

            </div>
        </div>
        <div class="my-3">

            <button type="submit" class="btn btn-success"><i class="bi bi-floppy2"></i> Enregistrer</button>
            <button class="btn btn-danger"><i class="bi bi-x-octagon-fill"></i> Annuler</button>
        </div>

    </form>
</div>

@endsection


