<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory;

    protected $table='article';
    protected $guarded=[];
    public function getCategory (){
         return $this->hasOne(Categorie::class,'id','categorie');
        //  return $this->hasOne(Categorie::class,'id','intitule');
    }
}
