<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Categorie;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::all();
        //dd($articles);

        return view('article.list', [
            'article' => $articles
        ]);
    }
    public function create()
    {
        //on affiche le formulaire de creation d'article tout en appelant le model categorie pour permettre la selection de la categorie

        $categories = Categorie::all();
        return view('article.create', [
            'categories' => $categories
        ]);
    }
    public function store(Request $request)
    {
        // recuperer les elements envoyés par l'utilisateur
        $article = $request->article;
        $categorie = $request->category;
        //Former le tableau de donnée et enregistrer(ne pas oublier le guarded)
        Article::create([
            'intitule' => $article,
            'categorie' => $categorie

        ]);
        //Rediriger vers la liste
      return redirect(route('article.index'))->with('success','Enregistrement effectué avec succès');
        // return back())->with('success','Enregistrement effectué');
    }

    public function edit($article){

//dd($article);
$categories = Categorie::all();
$art = Article::where('id',$article)->first();

return view('article.edit', [
    'categories' => $categories,
    'article'=>$art
]);
    }


    public function update(Request $request,$article)
    {
        // recuperer les elements envoyés par l'utilisateur
        $intitule = $request->article;
        $categorie = $request->category;
        //Former le tableau de donnée et enregistrer(ne pas oublier le guarded)
        Article::where('id',$article)->update([
            'intitule' => $intitule,
            'categorie' => $categorie
        ]);
        //Rediriger vers la liste
        return redirect(route('article.index'))->with('success','Modification effectuée avec succès');
    }
     public function delete($article){
     Article::where('id',$article)->delete();

     return redirect(route('article.index'))->with('success','Suppression effectuée avec succès');
     }
}
