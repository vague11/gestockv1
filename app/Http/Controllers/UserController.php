<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        $utilisateur = User::all();
        return view('user.list',[
            'user'=>$utilisateur
        ]);
    }

    public function create()
    {
        $utilisateur = User::all();
        return view('user.create', [
            'user' => $utilisateur
        ]);
    }
    public function store(Request $request)
    {
        // recuperer les elements envoyés par l'utilisat
        $nom = $request->name;
        $email = $request->mail;
        $pass = $request->password;
        //Former le tableau de donnée et enregistrer(ne pas oublier le guarded)
        User::create([
            'name' => $nom,
            'email' => $email,
            'password'=>$pass
        ]);
        //Rediriger vers la liste
        return redirect(route('user.index'));
    }
}
