<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CategorieController extends Controller
{
    public function index()
    {
        $categories = Categorie::all();
        //dd($categories);

        return view('category.list', [
            'categories' => $categories
        ]);
    }

    public function create()
    {

        return view('category.create');
    }

    public function store(Request $request)
    {
        // $request->validate([
        //     "intitule"=>"required",
        // ]);
        //une requete qui recupère la valeur envoyer par le formulaire dans une variable puis l'envoie a la table grace a create de blade

        $cate = $request->category;

        //Former le tableau de donnée et enregistrer(ne pas oublier le guarded)
        $rec = Categorie::create(
            [
                'intitule' => $cate
            ]
        );
        //Rediriger vers la liste
        return redirect(route('category.index'))->with('success', 'Enregistrement effectué avec succès');
        // return back())->with('success','Enregistrement effectué');
    }

    public function edit($categorie)
    {

        //dd($article);

        $categories = Categorie::where('id', $categorie)->first();

        return view('category.edit', [
            'categorie' => $categories,
        ]);
    }


    public function update(Request $request, $categorie)
    {
        // recuperer les elements envoyés par l'utilisateur

        $categori = $request->categories;
        //Former le tableau de donnée et enregistrer(ne pas oublier le guarded)
        Categorie::where('id', $categorie)->update([

            'intitule' => $categori
        ]);
        //Rediriger vers la liste
        return redirect(route('category.index'))->with('success', 'Modification effectuée avec succès');
    }
    public function delete($categorie)
    {
        Categorie::where('id', $categorie)->delete();

        return redirect(route('category.index'))->with('success', 'Suppression effectuée avec succès');
    }
}
