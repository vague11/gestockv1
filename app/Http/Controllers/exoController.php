<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class exoController extends Controller
{
    public function coucou() {
        $salutation= 'Bonjour';
        $name= 'Maelle B.';
        return view('presentation',['salut'=>$salutation,'nom'=> $name]);
    }

    public function customer(Request $request) {
        $salutation= $request-> get('salut');
        $name= $request-> get('nom');
        $age= $request-> get('age');
        return view('presentation', ['salut'=>$salutation,'nom'=>$name,'age'=>$age]);
    }
}
